#include <iostream>
#include "Cloner.h"

namespace Cloner {
void Cloner::clone(const json &src, json &dst, const ClonerOptions &options) {
  if (options.include_max_clients) {
    dst.at("RaceConfig").at("MaxClients") = src.at("RaceConfig").at("MaxClients");
  }
  if (options.include_cars) {
    dst.at("RaceConfig").at("Cars") = src.at("RaceConfig").at("Cars");
  }
  if (options.include_tyres) {
    dst.at("RaceConfig").at("LegalTyres") = src.at("RaceConfig").at("LegalTyres");
  }
  if (options.include_entry_lists) {
    if (!src.at("EntryList").empty() && src.at("EntryList").contains("CAR_0")) {
      dst.at("EntryList") = src.at("EntryList");
      bool src_is_new = src.at("EntryList").at("CAR_0").contains("CSPCarFlags");
      if (!src_is_new) {
        for (auto &it : dst.at("EntryList")) {
          it["ClassID"] = "00000000-0000-0000-0000-000000000000";
          it["ConnectAsSpectator"] = false;
          json csp_car_flags;
          csp_car_flags["allow_color_change"] = false;
          csp_car_flags["allow_immediate_refuel"] = false;
          csp_car_flags["allow_immediate_repair"] = false;
          csp_car_flags["allow_teleporting"] = false;
          csp_car_flags["block_joystick"] = false;
          csp_car_flags["block_keyboard"] = false;
          csp_car_flags["block_steering_wheel"] = false;
          csp_car_flags["force_headlights"] = false;
          it["CSPCarFlags"] = csp_car_flags;
          it["RaceNumber"] = 0;
        }
      }
    }
  }
  if (options.include_time_and_weather) {
    if (src.at("RaceConfig").at("Weather").size() != 1
        || dst.at("RaceConfig").at("Weather").size() != 1) {
      std::cerr << "More than one weather preset per race is not supported" << std::endl;
      throw std::exception{};
    }
    json src_weather = src.at("RaceConfig").at("Weather").at("WEATHER_0");
    json &dst_weather = dst.at("RaceConfig").at("Weather").at("WEATHER_0");
    bool src_is_new = !src_weather.contains("CMGraphics") && src_weather.contains("PracticeGraphicsName")
        && src_weather.contains("TrackWetness");
    bool dst_is_new = !dst_weather.contains("CMGraphics") && dst_weather.contains("PracticeGraphicsName")
        && dst_weather.contains("TrackWetness");
    if (!dst_is_new) {
      std::cerr << "Destination race is not in the new format" << std::endl;
      throw std::exception{};
    }
    if (src_is_new) {
      dst.at("RaceConfig").at("Weather").at("WEATHER_0") =
          src.at("RaceConfig").at("Weather").at("WEATHER_0");
    } else {
      // Only clone the known-compatible fields
      clone_subfield(src_weather, dst_weather, "BaseTemperatureAmbient");
      clone_subfield(src_weather, dst_weather, "BaseTemperatureRoad");
      clone_subfield(src_weather, dst_weather, "VariationAmbient");
      clone_subfield(src_weather, dst_weather, "VariationRoad");
      clone_subfield(src_weather, dst_weather, "Graphics");
      clone_subfield(src_weather, dst_weather, "Graphics", "PracticeGraphicsName");
      clone_subfield(src_weather, dst_weather, "WindBaseDirection");
      clone_subfield(src_weather, dst_weather, "WindBaseSpeedMax");
      clone_subfield(src_weather, dst_weather, "WindBaseSpeedMin");
      clone_subfield(src_weather, dst_weather, "WindVariationDirection");
      clone_subfield(src_weather, dst_weather, "CMWFXDate");
      clone_subfield(src_weather, dst_weather, "CMWFXDateUnModified");
      clone_subfield(src_weather, dst_weather, "CMWFXTimeMulti");
      clone_subfield(src_weather, dst_weather, "CMWFXType");
      clone_subfield(src_weather, dst_weather, "CMWFXDate", "CMWFXPracticeDate");
    }
  }
}

void Cloner::clone_subfield(const json &src_weather,
                            json &dst_weather,
                            const std::string &src_key,
                            const std::string &dst_key) {
  const std::string _dst_key = dst_key.empty() ? src_key : dst_key;
  dst_weather.at(_dst_key) = src_weather.at(src_key);
}
} // Cloner