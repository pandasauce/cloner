#include <QApplication>
#include "MainWindow.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  Cloner::MainWindow w;
  w.show();

  return QApplication::exec();
}
