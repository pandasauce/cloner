#ifndef CLONER_SRC_CLONER_H_
#define CLONER_SRC_CLONER_H_

#include <string>
#include <nlohmann/json.hpp>

using namespace nlohmann;

namespace Cloner {
class ClonerOptions {
 public:
  bool include_cars;
  bool include_tyres;
  bool include_entry_lists;
  bool include_max_clients;
  bool include_time_and_weather;
};

class Cloner {
 public:
  static void clone(const json& src, json& dst, const ClonerOptions& options);
 protected:
  static void clone_subfield(const json &src, json &dst, const std::string &src_key, const std::string &dst_key = "");
};

} // Cloner

#endif //CLONER_SRC_CLONER_H_
