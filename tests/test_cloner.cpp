#include <catch2/catch_test_macros.hpp>
#include <nlohmann/json.hpp>
#include <fstream>
#include "Cloner.h"

using namespace nlohmann;

TEST_CASE("Cloning with options") {
  auto file_src = std::ifstream ("../tests/data/race_src.json");
  auto file_dst = std::fstream("../tests/data/race_dst.json");
  if (!file_src.is_open() || !file_dst.is_open()) {
    throw std::exception{};
  }
  json src = json::parse(file_src);
  json dst = json::parse(file_dst);
  file_src.close();
  file_dst.close();

  REQUIRE(src != dst);
  REQUIRE(src.at("RaceConfig").at("Weather").at("WEATHER_0").at("BaseTemperatureAmbient").get<int>() != dst.at("RaceConfig").at("Weather").at("WEATHER_0").at("BaseTemperatureAmbient").get<int>());
  auto opts = Cloner::ClonerOptions(true, false, true, true, true);
  Cloner::Cloner::clone(src, dst, opts);
  REQUIRE(src.at("RaceConfig").at("Cars").get<std::string>() == dst.at("RaceConfig").at("Cars").get<std::string>());
  REQUIRE(src.at("RaceConfig").at("MaxClients").get<int>() == dst.at("RaceConfig").at("MaxClients").get<int>());
  // this is a negative test on purpose
  REQUIRE(src.at("RaceConfig").at("LegalTyres").get<std::string>() != dst.at("RaceConfig").at("LegalTyres").get<std::string>());
  REQUIRE(src.at("EntryList") != dst.at("EntryList"));
  REQUIRE(dst.at("EntryList").at("CAR_0").at("ClassID").get<std::string>() == "00000000-0000-0000-0000-000000000000");
  REQUIRE(src.at("EntryList").at("CAR_0").at("Skin").get<std::string>() == dst.at("EntryList").at("CAR_0").at("Skin").get<std::string>());
  REQUIRE(src.at("RaceConfig").at("Weather").at("WEATHER_0").at("BaseTemperatureAmbient").get<int>() == dst.at("RaceConfig").at("Weather").at("WEATHER_0").at("BaseTemperatureAmbient").get<int>());
}