# Assetto Corsa Server Manager (ACSM) Race Cloner

[![N|Solid](./doc/preview.png)](./doc/preview.png)

This software is written in C++ using Qt5 libraries on Linux.
It allows you to clone some options between race definitions in ACSM.

Main use case is defining a car pack (common in sim drifting) once and reusing it for multiple tracks without having to painstakingly click through things in the UI.

Gets particularly handy when you want to switch something up about the entry list and propagate that change to multiple race definitions.

Migrating ACSM v1.79 settings to v2.xx works too.

# Copyright

AGPL-3.0

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU Affero General Public License](./LICENSE) for more details.