#ifndef CLONER_SRC_MAINWINDOW_H_
#define CLONER_SRC_MAINWINDOW_H_

#include "ui_mainwindow.h"
#include <QDir>
#include <QMainWindow>

namespace Cloner {
 class MainWindow: public QMainWindow, public Ui::MainWindow {
 Q_OBJECT
  public:
   explicit MainWindow(QWidget * parent= nullptr, Qt::WindowFlags flags = {});
  protected:
   std::map<std::string, std::string> src_names_files;
   std::map<std::string, std::string> dst_names_files;
   static void load_directory_to(const QDir& directory, QListWidget* list, std::map<std::string, std::string>& map);
  private slots:
    void click_source();
    void click_destination();
    void click_refresh_destination();
    void click_clone();
};

} // Cloner

#endif //CLONER_SRC_MAINWINDOW_H_
