#include <QFileDialog>
#include <fstream>
#include "MainWindow.h"
#include "Cloner.h"
#include <nlohmann/json.hpp>
#include <iostream>

using namespace nlohmann;

namespace Cloner {
MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags), Ui::MainWindow() {
  this->setupUi(this);

  connect(this->btnSource, SIGNAL(clicked()), this, SLOT(click_source()));
  connect(this->btnDestination, SIGNAL(clicked()), this, SLOT(click_destination()));
  connect(this->btnClone, SIGNAL(clicked()), this, SLOT(click_clone()));
  connect(this->btnRefreshDestination, SIGNAL(clicked()), this, SLOT(click_refresh_destination()));
  connect(this->lsSource, &QListWidget::currentItemChanged, [this](QListWidgetItem *current, QListWidgetItem *previous) {
    if (current != nullptr && !current->text().isEmpty()) {
      auto file_name = this->src_names_files.at(current->text().toStdString());
      this->lbSource->setText(QString::fromStdString(file_name));
    }
  });
  connect(this->lsDestination, &QListWidget::currentItemChanged, [this](QListWidgetItem *current, QListWidgetItem *previous) {
    if (current != nullptr && !current->text().isEmpty()) {
      auto file_name = this->dst_names_files.at(current->text().toStdString());
      this->lbDestination->setText(QString::fromStdString(file_name));
    }
  });
}

void MainWindow::click_source() {
  auto directory_path = QFileDialog::getExistingDirectory(this, "Source directory", "~/");
  if (directory_path.isEmpty()) {
    return;
  }
  auto directory = QDir(directory_path);
  this->txtSource->setText(directory_path);
  load_directory_to(directory, this->lsSource, this->src_names_files);
}

void MainWindow::click_destination() {
  auto directory_path = QFileDialog::getExistingDirectory(this, "Destination directory", "~/");
  if (directory_path.isEmpty()) {
    return;
  }
  auto directory = QDir(directory_path);
  this->txtDestinaton->setText(directory_path);
  load_directory_to(directory, this->lsDestination, this->dst_names_files);
}

void MainWindow::click_clone() {
  this->progressBar->setValue(0);
  auto opts = ClonerOptions(
      this->cbCars->isChecked(),
      this->cbTyres->isChecked(),
      this->cbEntries->isChecked(),
      this->cbClients->isChecked(),
      this->cbWeather->isChecked()
  );

  if (!opts.include_cars && !opts.include_tyres && !opts.include_entry_lists && !opts.include_max_clients
      && !opts.include_time_and_weather) {
    return;
  }

  auto _kludge_src = this->lsSource->selectedItems();
  if (_kludge_src.size() != 1) {
    return;
  }
  auto _kludge_dst = this->lsDestination->selectedItems();
  if (_kludge_dst.size() != 1) {
    return;
  }
  auto src_name = _kludge_src.first()->text().toStdString();
  auto src_dir = QDir(this->txtSource->text());
  auto src_file = this->src_names_files.at(src_name);
  auto src_fh = std::ifstream(src_dir.absoluteFilePath(QString::fromStdString(src_file)).toStdString());
  auto src_json = json::parse(src_fh);
  src_fh.close();

  auto dst_name = _kludge_dst.first()->text().toStdString();
  auto dst_dir = QDir(this->txtDestinaton->text());
  auto dst_file = this->dst_names_files.at(dst_name);
  auto dst_fh = std::ifstream(dst_dir.absoluteFilePath(QString::fromStdString(dst_file)).toStdString());
  auto dst_json = json::parse(dst_fh);
  dst_fh.close();
//  dst_fh.seekp(0, std::ios::beg);
  this->progressBar->setValue(50);

  Cloner::clone(src_json, dst_json, opts);
  auto dst_fh_write = std::ofstream(dst_dir.absoluteFilePath(QString::fromStdString(dst_file)).toStdString());
  dst_fh_write << dst_json;
  dst_fh_write.flush();
  dst_fh_write.close();
  this->progressBar->setValue(100);
}

void MainWindow::load_directory_to(const QDir &directory, QListWidget *list, std::map<std::string, std::string> &map) {
  map.clear();
  list->clear();
  QStringList race_configs = directory.entryList(QStringList() << "*.json", QDir::Files);
  for (const auto &filename : race_configs) {
    auto filepath = directory.absoluteFilePath(filename);
    auto fs = std::ifstream(filepath.toStdString());
    try {
      json src = json::parse(fs);
      auto race_name = src.at("Name").get<std::string>();
      list->insertItem(0, QString::fromStdString(race_name));
      map.emplace(race_name, filename.toStdString());
    } catch (std::exception& err) {
      std::cerr << "Unable to load " << filepath.toStdString() << ", skipping" << std::endl;
    }
  }
}

void MainWindow::click_refresh_destination() {
  auto directory_path = this->txtDestinaton->text();
  if (directory_path.isEmpty()) {
    return;
  }
  auto directory = QDir(directory_path);
  load_directory_to(directory, this->lsDestination, this->dst_names_files);
}

} // Cloner